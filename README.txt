Description:
------------
This module extends the webform module to easily allow submitting a webform 
to SugarCRM.  It was developed after an attempt to extend the sugarform 
module (http://drupal.org/project/sugarform) started to get closer and closer
to webform.

URGENT: Opting does not work correctly without an edit to the SugarCRM source
code (SugarCRM bug 21851 has been filed).  See INSTALL.txt.

Thanks to Didymo Designs (http://www.didymodesigns.com.au/) for the original
work on the original sugarform module.
