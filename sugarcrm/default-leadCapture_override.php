<?php

# set up the array of usernames and hashed password
$users = array(
	'acquia.com' => array('name'=>'acquia.com', 'pass'=>'292636ae210da853937e52ed9fe2ed30')
);

# identify the web site user by id and assigned the lead to them
$assigned_user_id = $current_user->retrieve_user_id($users[$_REQUEST['user']]['name']);
if( !isset($_POST['assigned_user_id']) || !empty($_POST['assigned_user_id']) ){
	$_POST['assigned_user_id'] = $assigned_user_id;
}

# if we have an Opt-In field, we want to get rid of any Opt-Out field that we may have
if ( !empty($_POST['email_opt_in']) ) {
	unset($_POST['email_opt_out']);
} 

# convert opt out to an explicit binary boolean
if ( isset($_POST['email_opt_out']) ) {
	$_POST['email_opt_out'] = 1;
}
